package com.lpiem.pokemon.fragments.account;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.lpiem.pokemon.R;
import com.lpiem.pokemon.fragments.BaseFragment;

/**
 * Created by lionelbanand on 05/12/2017.
 */

public class CreateAccountFragment extends BaseFragment {

    Button btnNext;

    public static CreateAccountFragment newInstance() {

        Bundle args = new Bundle();

        CreateAccountFragment fragment = new CreateAccountFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_create_account, container, false);
        btnNext = v.findViewById(R.id.next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            changeFragment(ValidateAccount.newInstance());
            }
        });
        return v;


    }


}
