package com.lpiem.pokemon.fragments;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.lpiem.pokemon.R;
import com.lpiem.pokemon.activities.BaseActivity;
import com.lpiem.pokemon.activities.MainActivity;

/**
 * Created by lionelbanand on 08/12/2017.
 */

public abstract class BaseFragment extends Fragment {
    BaseActivity context;


    public void changeFragment(BaseFragment f) {
        context.changeFragment(f);
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = (MainActivity) activity;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (MainActivity)context;
    }
}
