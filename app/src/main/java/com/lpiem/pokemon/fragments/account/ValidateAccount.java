package com.lpiem.pokemon.fragments.account;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lpiem.pokemon.R;
import com.lpiem.pokemon.fragments.BaseFragment;

/**
 * Created by lionelbanand on 05/12/2017.
 */

public class ValidateAccount extends BaseFragment {

    public static ValidateAccount newInstance() {
        
        Bundle args = new Bundle();
        
        ValidateAccount fragment = new ValidateAccount();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_validate_account, container, false);

        AccountManager.getInstance().callWS();

        return v;
    }
}
